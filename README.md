# QR-codes

## Generating QR-codes in Python

In the ipynb notebook you can see instructions on how to create qr-codes in python.

<img src='site.png' width=250>


## Interactive qr-code window with coordinates and bounding box

<img src='qr-code.png' width=250>