import cv2
img = cv2.imread("site.png") #read the generated image
detector = cv2.QRCodeDetector() #init detector of qr
data, box, qr_code = detector.detectAndDecode(img) #decoded data, array of vertices, straight output img qrcode
if box is not None:
    print(f'QR-Code:{data}') #output data
    n = len(box) #length of bounding box
    for i in range(n):
        p1 = tuple(box[i][0])
        p2 = tuple(box[(i+1)%n][0]) #divide by n because print by lines
        cv2.line(img, p1, p2, color=(255, 0, 0), thickness = 2) #draw a line segment of color blue
cv2.imshow("img", img) #display the results
cv2.waitKey(0)
cv2.destroyAllWindows() #stop when key is pressed


