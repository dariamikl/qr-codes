import cv2
cap = cv2.VideoCapture(0) #initialize cv2 for capturing video
detector = cv2.QRCodeDetector() #detector for reading qr codes
while True:
    _, img = cap.read() #read image from the camera
    data, bbox, _ = detector.detectAndDecode(img) #use detector in this area
    if bbox is not None: #look for qr code
        for i in range(len(bbox)):
            cv2.line(img, tuple(bbox[i][0]), tuple(bbox[(i+1) % len(bbox)][0]), color=(255, 0, 0), thickness=2) #draw the bounding box
        if data:
            print("[+] QR Code detected, data:", data)
    cv2.imshow("img", img)  #display the image  
    if cv2.waitKey(1) == ord("q"): #exit on q
        break
cap.release()
cv2.destroyAllWindows() #delete the window